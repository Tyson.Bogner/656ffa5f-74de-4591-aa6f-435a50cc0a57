import React, { useEffect, useState } from "react";

import Events from "./components/Events";
import Nav from "./components/Nav";

const API_URL = "https://tlv-events-app.herokuapp.com/events/uk/london";

function App() {
  const [events, setEvents] = useState([]);
  const [searchTerm, setSearchTerm] = useState("");

  const fetchEvents = async (title) => {
    try {
      const response = await fetch(API_URL);
      const events = await response.json();

      const eventsSorted = events.sort((a, b) => {
        let dateA = a.date.split("T");
        let dateB = b.date.split("T");

        return new Date(dateA[0]) - new Date(dateB[0]);
      });

      setEvents(eventsSorted);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    fetchEvents();
  }, []);

  return (
    <main>
      <Nav setSearchTerm={setSearchTerm} />
      <Events events={events} searchTerm={searchTerm} />
    </main>
  );
}

export default App;
